module KDL.Doc
  ( Document(..)
  , Identifier(..)
  , Node(..)
  , Value(..)
  , ValueType(..)
  )
  where

import Prelude
import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Map (Map)
import Data.Maybe (Maybe)
import Data.Show.Generic (genericShow)
import Data.Tuple.Nested (Tuple2)

newtype Document
  = NewDocument
  { docNodes :: List Node
  }

derive instance eqDocument :: Eq Document

data Node
  = Node
    { nodeAnn :: Maybe Identifier
    , nodeName :: Identifier
    , nodeArgs :: List Value
    , nodeProps :: Map Identifier Value
    , nodeChildren :: List Node
    }

derive instance eqNode :: Eq Node

newtype Identifier
  = Identifier String

emptyIdentifier :: Identifier
emptyIdentifier = Identifier ""

derive instance eqIdentifier :: Eq Identifier

derive instance genericIdentifier :: Generic Identifier _

derive instance ordIdentifier :: Ord Identifier

instance showIdentifier :: Show Identifier where
  show = genericShow

data Content
  = NodeValue { getValue :: Value }
  | NodeProperty { getProp :: Tuple2 Identifier Value }

derive instance eqContent :: Eq Content

data Value
  = Value
    { valueAnn :: Maybe Identifier
    , valueExp :: ValueType
    }

derive instance eqValue :: Eq Value

derive instance genericValue :: Generic Value _

instance showValue :: Show Value where
  show = genericShow

data ValueType
  = StringValue String
  | IntegerValue Int
  | SciValue Number
  | BooleanValue Boolean
  | NullValue

derive instance eqValueType :: Eq ValueType

derive instance genericValueType :: Generic ValueType _

instance showValueType :: Show ValueType where
  show = genericShow
