module KDL.Parser where

import Prelude

import Control.Monad.Error.Class (throwError)
import Data.Array (notElem, (..))
import Data.CodePoint.Unicode (isHexDigit)
import Data.Map as M
import Data.Maybe (Maybe(..))
import KDL.Doc (Document(..), Identifier(..), Node(..), Value(..), ValueType(..))
import Parsing (Parser, ParserT)
import Parsing.Combinators (choice, many, manyTill, optional, try, withErrorMessage, (<?>), (<|>))
import Parsing.String (char, eof, match, satisfy, satisfyCodePoint, string)
import Parsing.String.Basic (noneOf, number)
import Prim.Boolean (True)

label :: forall m s a. String -> ParserT s m a -> ParserT s m a
label = flip withErrorMessage

newline :: Parser String Unit
newline = void $ char '\n'

escline :: Parser String Unit
escline = parse <?> "Escape Line"
  where
  parse = do
    _ <- string "\\"
    _ <- many ws
    _ <- try linebreak <|> lineComment
    pure $ unit

linespace :: Parser String Unit
linespace = parse <?> "Line Space"
  where
  parse = try linebreak <|> try ws <|> try lineComment

linebreak :: Parser String Unit
linebreak = parse <?> "Newline"
  where
  parse = do
    _ <- choice $ charList
    pure $ unit

  charList =
    [ char '\r' <?> "carriage return"
    , char '\n' <?> "newline"
    , char '\x85' <?> "next line"
    , char '\x000c' <?> "form feed"
    , char '\x2028' <?> "line seperator"
    , char '\x2029' <?> "paragraph seperator"
    ]

ws :: Parser String Unit
ws = parse <?> "Whitespace"
  where
  parse = do
    _ <- bom <|> hspacechar <|> blockComment
    pure $ unit

bom :: Parser String Unit
bom = void $ char '\xfeff' <?> "BOM"

hspacechar :: Parser String Unit
hspacechar =
  label "Newline"
    $ do
        choice $ map void charList
  where
  charList =
    [ char '\x0009' <?> "character tabulation"
    , char '\x0020' <?> "space"
    , char '\x00A0' <?> "bo-break space"
    , char '\x1680' <?> "ogham space mark"
    , char '\x2000' <?> "en quad"
    , char '\x2001' <?> "em quad"
    , char '\x2002' <?> "en space"
    , char '\x2003' <?> "em space"
    , char '\x2004' <?> "three-per-em space"
    , char '\x2005' <?> "four-per-em space"
    , char '\x2006' <?> "six-per-em space"
    , char '\x2007' <?> "figure space"
    , char '\x2008' <?> "punctuation space"
    , char '\x2009' <?> "thin space"
    , char '\x200A' <?> "hair space"
    , char '\x202F' <?> "narrow no-break space"
    , char '\x205F' <?> "medium mathmatical space"
    , char '\x3000' <?> "ideographic space"
    ]

{- 
lineComment :: Parser ()
lineComment = L.skipLineComment "//" >> (void newline <|> eof)
-}
lineComment :: Parser String Unit
lineComment = skipLineComment "//" *> (void newline <|> eof)
  where
  skipLineComment start = string start *> many notLineFeed

  notLineFeed = satisfy (\x -> x /= '\n')

{-
blockComment :: Parser ()
blockComment = L.skipBlockCommentNested "/*" "*/"
-}
blockComment :: Parser String Unit
blockComment = skipBlockCommentNested "/*" "*/"

skipBlockCommentNested :: String -> String -> Parser String Unit
skipBlockCommentNested start end = startComment *> void (manyTill contents endComment)
  where
  startComment = string start

  endComment = string end

  contents = skipBlockCommentNested start end <|> void anySingle

document :: Parser String Document
document = do
  nodes <- many node
  _ <- eof
  pure $ NewDocument { docNodes: nodes }

node :: Parser String Node
node = do
  pure
    $ Node
        { nodeAnn: nodeAnn
        , nodeArgs: nodeArgs
        , nodeChildren: nodeChildren
        , nodeName: nodeName
        , nodeProps: nodeProps
        }
  where
  nodeAnn = Nothing

  nodeArgs = mempty

  nodeChildren = mempty

  nodeName = Identifier ""

  nodeProps = M.empty

between :: forall a. Char -> Char -> Parser a
between start end = do
  void (char start)
  i <- manyTill end
  pure i

anySingle :: Parser String
anySingle = satisfy (const true)

rawstring :: Parser String
rawstring =
  label "Raw String"
    $ do
        void (char 'r')
        hs <- manyTill (char '#')
        void (char '"')
        s <- manyTill anySingle (string ('"' <> hs))
        pure s

escstring :: Parser String
escstring =
  label "String"
    $ do
        append <$> (char '"' *> manyTill character (char '"'))

character :: Parser String
character = void (char '\\') *> escape <|> nonescape

escape :: Parser String
escape = escapesChoice <|> uescape
  where
  escapesChoice = do
    e <-
      choice
        [ '\x08' <$ char 'b'
        , '\x09' <$ char 't'
        , '\x0A' <$ char 'n'
        , '\x0C' <$ char 'f'
        , '\x0D' <$ char 'r'
        , '\x22' <$ char '\"'
        , '\x2F' <$ char '/'
        , '\x5C' <$ char '\\'
        ]
    pure e

--FIXME
{--

--}
uescape :: Parser String
uescape = do
  _ <- void $ string "{"
  u <- ?anyHexDigits
  _ <- void $ char '}'
  pure u

nonescape :: Parser String
nonescape = do
  c <- noneOf ("\\\"" :: Array Char)
  pure c

anystring :: Parser String
anystring = try rawstring <|> escstring

name :: Parser Identifier
name = Identifier <$> (try anystring <|> identifier)

-- Needs to be fixed a bit, not a straight copy
identifier :: Parser String
identifier =
  label "Identifier"
    $ do
        i <- satisfyCodePoint iichar
        is <- many (satisfyCodePoint ichar)
        let
          result = i <> is
        case result of
          "true" -> throwError "keyword true in identifier"
          "false" -> throwError "keyword true in identifier"
          "null" -> throwError "keyword true in identifier"
          _ -> pure result
  where
    ichar c = 
      (c > '\x20')
        && ( c <= "\x10FFFF")
        && ( c `notElem` ("\\/(){}<>;[]=,\""))
        && not (match linespace ("" <> c))
    iichar c = ichar c && c `notElem` ['0' .. '9']

binary :: Parser Int
binary = do
  _ <- char '0' 
  _ <- char 'b' 
  _ <- number

typeAnnotation :: Parser Identifier
typeAnnotation =
  label "Type Annotation"
    $ do
        void (char '(')
        i <- name
        void (char ')')
        pure i

value :: Parser Value
value =
  label "Value"
    $ do
        valueAnn <- optional typeAnnotation
        valueExp <-
          choice
            [ IntegerValue <$> try binary <?> "Binary"
            ]
        pure Value { valueAnn: valueAnn, valueExp: valueExp }
