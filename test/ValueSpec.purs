module Test.ValueSpec
  ( valueSpec
  ) where

import Prelude
import Data.Either (Either(..), isLeft)
import Parsing (runParser)
import Parsing.String (string)
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

valueSpec :: Spec Unit
valueSpec = do
  describe "It Should Pass" do
    it "should parse ab"
      $ do
          shouldParse "ab" "ab"
    it "should not parse ac"
      $ do
          shouldNotParse "ac"
  where
  parser = string "ab"

  testParser parserToTest val = runParser val parserToTest

  shouldParse val expected = (testParser parser val) `shouldEqual` (Right expected)

  shouldNotParse val = isLeft (testParser parser val) `shouldEqual` true
