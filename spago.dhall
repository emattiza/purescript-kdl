{ name = "my-project"
, dependencies =
  [ "aff"
  , "console"
  , "effect"
  , "lists"
  , "maybe"
  , "tuples"
  , "prelude"
  , "parsing"
  , "spec"
  , "ordered-collections"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
